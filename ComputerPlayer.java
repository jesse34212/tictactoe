package AssignTicTac;
public class ComputerPlayer extends Player {

	@Override
	public int makeMove(TicTacToeImpl ticTac, Player enemy) {
		int result = 0;
		// 1. Check to see if I have a winning possibility
		result = checkForWinningMove(ticTac);
		// 2. Check to see if my opponent has a winning move
		if (result == 0) {
			result = checkForWinningMove(ticTac, enemy);
		}

		// 3. Check for the middle
		if (result == 0) {
			result = checkForMiddle(ticTac);
		} 
		

		// 4. Check for random corners
		if (result == 0) {
			result = checkForRandomCorner(ticTac);
		}

		// 5. check for random middle edges
		if (result == 0) {
			result = checkForRandomEdgeMiddle(ticTac);
		}

		setMyMove(result);
		// return the position on the board that the move will be made
		return result;
	}

	private int checkForWinningMove(TicTacToeImpl ticTac) {
		return checkForWinningMove(ticTac, this);
	}

	private int checkForWinningMove(TicTacToeImpl ticTac, Player player) {
		int[][] winningCombinations = ticTac.getWinningCombination();
		int result = 0; // returning 0 means there is not a winning move
		for (int[] singleWinCombination : winningCombinations) {
			if (player instanceof ComputerPlayer) {
				result = player.verify(singleWinCombination, player, ticTac);
			} else {
				result = verify(singleWinCombination, player, ticTac);
			}
			if (result != 0) {// means it will be 1-9 (a position on the board)
				break;
			}
		}
		return result;
	}

	public int verify(int[] singleWinCombination, Player player,
			TicTacToeImpl ticTac) {
		int result = 0;
		boolean[] compared = new boolean[3];
		int[] myMovesMade = player.getMyMoves();
		/*
		 * iterate through moves that have been made in search for a winning
		 * combination
		 */

		for (int myMoves : myMovesMade) {
			if (myMoves != 0) {
				if (myMoves == singleWinCombination[0]) {
					compared[0] = true;
				} else if (myMoves == singleWinCombination[1]) {
					compared[1] = true;
				} else if (myMoves == singleWinCombination[2]) {
					compared[2] = true;
				}
			}
		}
		if (compared[0]) {
			if (compared[1]) {
				if (ticTac.getPositionIsUnoccupied(singleWinCombination[2])) {
					result = singleWinCombination[2];
				}
			} else if (compared[2]) {
				if (ticTac.getPositionIsUnoccupied(singleWinCombination[1])) {
					result = singleWinCombination[1];
				}
			}
		} else if (compared[1] && compared[2]) {
			if (ticTac.getPositionIsUnoccupied(singleWinCombination[0])) {
				result = singleWinCombination[0];
			}// }
		}
		player.notFirstMove();
		// returning 0 means that we are not 1 move away from winning or losing
		return result;
	}

	private int checkForMiddle(TicTacToeImpl ticTac) {
		return (ticTac.getPositionIsUnoccupied(TicTacToeImpl.MIDDLE) ? TicTacToeImpl.MIDDLE : 0);
	}

	private int checkForRandomCorner(TicTacToeImpl ticTac) {
		int result = 0;
		int randomPosition = (int) ((Math.random() * 4));
		for (int i = 0; i < TicTacToeImpl.CORNERS.length; i++) {
			int temp = TicTacToeImpl.CORNERS[randomPosition];
			if (ticTac.getPositionIsUnoccupied(temp)) {
				result = temp;
				break;
			}
			if (randomPosition == 3) {
				randomPosition = 0;
			} else {
				randomPosition++;
			}
			// randomPosition = (randomPosition == 3) ? 0 : randomPosition + 1;

		}
		return result;
	}

	private int checkForRandomEdgeMiddle(TicTacToeImpl ticTac) {
		Integer randomPosition = (int) ((Math.random() * 4));
		int result = 0;
		for (int i = 0; i < TicTacToeImpl.EDGE_MIDDLE.length; i++) {
			if (ticTac
					.getPositionIsUnoccupied(TicTacToeImpl.EDGE_MIDDLE[randomPosition])) {
				result = TicTacToeImpl.EDGE_MIDDLE[randomPosition];
				break;
			} else if (randomPosition == 3) {
				randomPosition = 0;
			} else {
				randomPosition++;
			}
//			randomPosition = (randomPosition == 3) ? 0 : randomPosition + 1;
			//if(randomPosition==3) ? randomPosition = 0 : randomPosition++;

		}
		return result;
	}
}
