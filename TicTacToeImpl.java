package AssignTicTac;
public class TicTacToeImpl {

	public static final String DIVIDER = "-----------";
	public static final String LINE_DIVIDER = DIVIDER+DIVIDER+DIVIDER;
	public static final String SPACER = "          ";

	public static final int TOP_LEFT = 1;
	public static final int TOP_MIDDLE = 2;
	public static final int TOP_RIGHT = 3;
	public static final int MIDDLE_LEFT = 4;
	public static final int MIDDLE = 5;
	public static final int MIDDLE_RIGHT = 6;
	public static final int BOTTOM_LEFT = 7;
	public static final int BOTTOM_MIDDLE = 8;
	public static final int BOTTOM_RIGHT = 9;

	public static int POSITION_ONE = 1;
	public static int POSITION_TWO = 2;
	public static int POSITION_THREE = 3;
	public static int POSITION_FOUR = 4;
	public static int POSITION_FIVE = 5;
	public static int POSITION_SIX = 6;
	public static int POSITION_SEVEN = 7;
	public static int POSITION_EIGHT = 8;
	public static int POSITION_NINE = 9;

	public static final int[][] WINNING_COMBINATIONS = { { 1, 2, 3 },
			{ 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 },
			{ 1, 5, 9 }, { 3, 5, 7 } };

	public String[][] TIC_TAC_TOE_BOARD = { { " ", " " }, { " ", " " },
			{ " ", " " } };
	// For decisions where middle is occupied
	public static final int[][] WINNING_COMBINATIONS_EDGES = { { 1, 2, 3 },
			{ 1, 4, 7 }, { 3, 6, 9 }, { 7, 8, 9 } };
	public static final int[] CORNERS = { 1, 3, 7, 9 };
	public static final int[] EDGE_MIDDLE = { 2, 4, 6, 8 };
	private PlayerPosition[] boardOccupancy = { PlayerPosition.UNDEFINED,
			PlayerPosition.UNDEFINED, PlayerPosition.UNDEFINED,
			PlayerPosition.UNDEFINED, PlayerPosition.UNDEFINED,
			PlayerPosition.UNDEFINED, PlayerPosition.UNDEFINED,
			PlayerPosition.UNDEFINED, PlayerPosition.UNDEFINED,
			PlayerPosition.UNDEFINED, };

	private PlayerPosition playerPosition;
	private PlayerPosition winner;
	private GameOutcome gameWinner;
	Player p1;
	Player p2;

	public TicTacToeImpl(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
	}

	public void play() {
		gameWinner = GameOutcome.CONTINUE;
		while (GameOutcome.CONTINUE.equals(gameWinner)) {
			System.out.println(this+"\n"+LINE_DIVIDER+"\n"+LINE_DIVIDER+"\n");
			System.out.print("Player 1 please make a selection: ");
			setMove(PlayerPosition.X, p1.makeMove(this, p2));
			if (checkForTie()) {
				gameWinner = GameOutcome.TIE;
				break;
			}

			if (checkForWin()) {
				gameWinner = GameOutcome.PLAYER_ONE_WINS;
				break;
			}
			System.out.println(this+"\n"+LINE_DIVIDER+"\n"+LINE_DIVIDER+"\n");
			System.out.print("Player 2 please make a selection: ");
			setMove(PlayerPosition.O, p2.makeMove(this, p1));
			if (checkForTie()) {
				gameWinner = GameOutcome.TIE;
				break;
			}

			if (checkForWin()) {
				gameWinner = GameOutcome.PLAYER_TWO_WINS;
				break;
			}
		}
	}

	public int[][] getWinningCombination() {
		return WINNING_COMBINATIONS;
	}

	public void setMove(PlayerPosition playerPosition, int boardOccupancyIndex) {
		if (boardOccupancyIndex==0) {
			gameWinner = GameOutcome.TIE;
		} else {
			boardOccupancy[boardOccupancyIndex] = playerPosition;
		}
	}

	private boolean checkForWin() {
		boolean result = false;
		for (int[] winningCombination : WINNING_COMBINATIONS) {
			if ((PlayerPosition.X.equals(boardOccupancy[winningCombination[0]]))
					&& (PlayerPosition.X.equals(boardOccupancy[winningCombination[1]]))
					&& (PlayerPosition.X.equals(boardOccupancy[winningCombination[2]]))) {
				result = true;
				this.winner = PlayerPosition.X;

			} else if ((PlayerPosition.O.equals(boardOccupancy[winningCombination[0]]))
					&& (PlayerPosition.O.equals(boardOccupancy[winningCombination[1]]))
					&& (PlayerPosition.O.equals(boardOccupancy[winningCombination[2]]))) {
				result = true;
				this.winner = PlayerPosition.O;
			}
		}
		return result;
	}

	private boolean checkForTie() {
		boolean result = true;
		for (int boardItr = 1; boardItr < 10; boardItr++) {
			if ((!PlayerPosition.X.equals(boardOccupancy[boardItr]))
					&& (!PlayerPosition.O.equals(boardOccupancy[boardItr]))) {
				result = false;
				break;
			}
		}
		return result;
	}

	public GameOutcome getGameWinner() {
		return gameWinner;
	}

	public boolean getPositionIsUnoccupied(int position) {
		return ((!PlayerPosition.X.equals(boardOccupancy[position]))
				&& (!PlayerPosition.O.equals(boardOccupancy[position])));
	}

	public PlayerPosition[] getBoardOccupancy() {
		return boardOccupancy;
	}

	private String playerString(PlayerPosition p) {
		String player;
		if (PlayerPosition.O.equals(p)) {
			player = "O";
		} else if (PlayerPosition.X.equals(p)) {
			player = "X";
		} else {
			player = " ";
		}
		return player;
	}

	@Override
	public String toString() {
		String line1 = "\n  " + playerString(boardOccupancy[POSITION_ONE])
				+ " | " + playerString(boardOccupancy[POSITION_TWO]) + " | "
				+ playerString(boardOccupancy[POSITION_THREE]) + SPACER
				+ "  1 | 2 | 3 \n ";
		String line2 = DIVIDER + SPACER + DIVIDER + "\n";
		String line3 = "  " + playerString(boardOccupancy[POSITION_FOUR])
				+ " | " + playerString(boardOccupancy[POSITION_FIVE]) + " | "
				+ playerString(boardOccupancy[POSITION_SIX]) + SPACER
				+ "  4 | 5 | 6 \n ";
		String line4 = DIVIDER + SPACER + DIVIDER + "\n";
		String line5 = "  " + playerString(boardOccupancy[POSITION_SEVEN])
				+ " | " + playerString(boardOccupancy[POSITION_EIGHT]) + " | "
				+ playerString(boardOccupancy[POSITION_NINE]) + SPACER
				+ "  7 | 8 | 9 \n ";
		return line1 + line2 + line3 + line4 + line5;
	}

}
