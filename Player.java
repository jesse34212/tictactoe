package AssignTicTac;

public abstract class Player{
	protected int[] myMoves={0,0,0,0,0,0,0,0,0,0};
	protected boolean firstMove = true;
		
	public abstract int makeMove(TicTacToeImpl ticTac, Player enemy);
	
	public int[] getMyMoves() {
			return myMoves;
	}
	
	public void setMyMove(int move){
		myMoves[move]=move;
	}
	
	public int verify(int[] i, Player player, TicTacToeImpl ticTac){
		return 0;
	}
	
	public boolean firstMove(){
		return firstMove;
	}
	
	public void notFirstMove(){
		firstMove = false;
	}
}
