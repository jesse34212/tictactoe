package AssignTicTac;

import java.util.Scanner;

public class HumanPlayer extends Player {

	@Override
	public int makeMove(TicTacToeImpl ticTac, Player enemy) {
		if (!GameOutcome.CONTINUE.equals(ticTac.getGameWinner())) {
			return 0;
		}

		int myMove = 0;
		Scanner scanner = new Scanner(System.in);
		myMove = (int) scanner.nextInt();
		if (!ticTac.getPositionIsUnoccupied(myMove)) {
			while (!ticTac.getPositionIsUnoccupied(myMove)) {
				System.out.println("Position " + myMove
						+ " is already occupied." + ticTac
						+ "\n Please try again: ");
				scanner = new Scanner(System.in);
				myMove = (int) scanner.nextInt();
			}
		}
		setMyMove(myMove);
		System.out.println("You chose " + myMove + " as your move.");
		return myMove;
	}
}
