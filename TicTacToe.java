package AssignTicTac;
public class TicTacToe {

	public static void main(String[] args) {
		TicTacToeImpl game;
		/*= new TicTacToeImpl(new HumanPlayer(),
				new ComputerPlayer());
				*/

		if (args.length == 0) {
			game = new TicTacToeImpl(new HumanPlayer(), new HumanPlayer());
		} else if ((args.length == 1) && ("-c".equals(args[0]))) {
			game = new TicTacToeImpl(new ComputerPlayer(), new ComputerPlayer());
		} else if ((args.length == 2) && ("-c".equals(args[0]))
				&& ("1".equals(args[1]))) {
			game = new TicTacToeImpl(new ComputerPlayer(), new HumanPlayer());
		} else { //this will be our default game
			game = new TicTacToeImpl(new HumanPlayer(), new ComputerPlayer());
		}
		game.play();
		if (game.getGameWinner() == GameOutcome.PLAYER_ONE_WINS) {
			System.out.println("Player 1 wins!" + game);
		} else if (game.getGameWinner() == GameOutcome.PLAYER_TWO_WINS) {
			System.out.println("Player 2 Wins!" + game);
		} else if (game.getGameWinner() == GameOutcome.TIE) {
			System.out.println("Tie!" + game);
		}
	}
}
