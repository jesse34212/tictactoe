package AssignTicTac;

public enum GameOutcome {
	PLAYER_ONE_WINS,
	PLAYER_TWO_WINS,
	TIE,
	CONTINUE
}
