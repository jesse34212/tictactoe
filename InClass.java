package AssignTicTac;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

//"Serializable"
//ObjectOutputStream ObjectInputStream
//Scanner while scanner.hasNext()

public class InClass {
	int number=0;
	public static void main(String[] args) throws FileNotFoundException {
		byte[] thisByte = {0x02, 0x00, 0x00, 0x08, (byte) 0xFF, 0x13, (byte) 0xF6, 0x01, 0x01, 0x02, 0x02, (byte) 0xF6};
		byte[] thisByte2 = {0x02, 0x00, 0x00, 0x0C, (byte)0xFF, 0x13, (byte)0xF6, 0x06, 0x06, 0x06, 0x01, 0x02, 0x03, 0x04, 0x03, (byte)0xE7};
		InClass inClass = new InClass();
		inClass.byteTest(thisByte);		
	}
//	int number;
//	public static void main(String[] args) throws FileNotFoundException {
//		byte[] thisByte = {0x02, 0x00, 0x00, 0x08, (byte) 0xFF, 0x13, (byte) 0xF6, 0x06, 0x06, 0x02, 0x02, (byte) 0xF6};
//		InClass inClass = new InClass();
//		inClass.scannerTest();
//		
//	}
//	
//	public void scannerTest() {
//		try{
//			Scanner scanner = new Scanner(new File("/Users/jessehanna/Desktop/testfile.txt"));
//			while (scanner.hasNext()){
//				System.out.println(scanner.next());
//			}
//			scanner.close();
//			FileWriter fo = new FileWriter(new File("/Users/jessehanna/Desktop/testfile.txt"), true);
//			fo.write("Writing to the file");
//			fo.close();
//			scanner = new Scanner(new File("/Users/jessehanna/Desktop/testfile.txt"));
//			while (scanner.hasNext()){
//				System.out.println(scanner.next());
//			}
//			scanner.close();
//		} catch (FileNotFoundException e){
//			System.out.println("something's wrong");
//		
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//			}
	public void byteTest2(byte[] parser){
		byte[] test = {0x11, (byte)0xFF};
		for (byte t : test){
			int number = (int) t;
			System.out.println(number);
		}
	}
	
	public void byteTest(byte[] parser){
		int i = 0; 
		for (byte next:parser){
			//first index of byte array is not included in checksum
			if (i >1){
				number += next;
			}
			i++;
		}
		number = number & 0xFF;
//		number = Integer.toHexString(number);
		System.out.println(Integer.toHexString(number));
	}
}
